import discord
import threading
import json
import subprocess
import pafy
import re
import sys
import os
import urllib.request, urllib.error, urllib.parse
import urllib.request, urllib.parse, urllib.error
import vlc
import asyncio
import time
import random
import glob
import shlex
from cleverbot import Cleverbot

if not discord.opus.is_loaded():
	discord.opus.load_opus('libopus-0.dll')

client = discord.Client()
last_channel = None
voice = None
player = None
cb =Cleverbot()

dir = os.path.dirname(__file__)

pl = []
titles = []
pretty_pl = []
votes = 0

for file in os.listdir("musica/sc/mandadas"):
	try:
		os.remove("musica/sc/mandadas"+file)
	except:
		pass
for f in os.listdir("musica/sc/"):
	try:
		os.remove("musica/sc/"+file)
	except:
		pass

def playSong(songPath):
	global voice
	global player
	global votes
	votes = 0
	player = voice.create_ffmpeg_player(songPath,after=onSongEnd)
	player.start()
	
def onSongDl(song,title,author):
	print(song)
	pl.append(song)
	pretty_pl.append(title + ' requested by ' + author.display_name)
	if len(pl) == 1:
	   # os.system('cvlc '+ ' -q ' +'musica/'+song + ' --play-and-exit')
		#popenAndCall(onSongEnd,['cvlc','-q', 'musica/'+song + ' --play-and-exit'])
		pThread = playSong(os.path.join(dir,'musica',song))

	return


def bajarAudio(audiostream,search,title,author):
	search = re.sub(' ','_',search)
	filename = os.path.join(dir,'musica', search + '.' + audiostream.extension)
	#filename = 'musica\'+search+'.'+audiostream.extension
	if not os.path.isfile(filename):
		audiostream.download(filepath=filename)
	print('#yalobaje')
	onSongDl(search+'.'+audiostream.extension,title,author)

	return

def onSongIn(search,author):
    if 'https://www.youtube.com' in search:
        ytlink = search.strip()
    else:
        ytlink = ytQuery(search)
    try:
        getAudio(ytlink,search,author)
    except:
        print ('se cago algo, no te bajo ' + search + ' una mierda')
    return



def getAudio(link,search,author):

	video = pafy.new(link)
	best = video.getbestaudio()
	audiostreams = video.audiostreams
	bajar = best
	for a in audiostreams:
		print('pija')
		if a.bitrate == '128k':
			bajar = a
			break

	thread2 = threading.Thread(target= bajarAudio,args=(bajar,search,video.title, author))
	thread2.start()
	return






def onSongEnd():
	global pl
	global pretty_pl
	print('onSongEnd')
	if len(pl) > 0:
		os.remove(os.path.join(dir,"musica",pl[0]))
		del pl[0]
		del pretty_pl[0]
		if len(pl) > 0:
			song = pl[0]
			threadPlayer = playSong(os.path.join(dir,'musica',song))
		#popenAndCall(onSongEnd,['cvlc', '-q', 'musica/'+song + ' --play-and-exit'])
	   # print 'play'




	return


def ytQuery(query):

	query_string = urllib.parse.urlencode({"search_query" : query})
	html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
	montoto = html_content.read().decode('utf-8')
	search_results = re.findall(r'href=\"\/watch\?v=(.{11})', montoto)
	return "http://www.youtube.com/watch?v=" + search_results[0]



def bajarSC(link,author):
	os.system("scdl -l "+link + " --onlymp3 --path musica/sc/")
	dled = os.listdir("musica/sc/")[0]
	if dled == "mandadas":
		dled = dled = os.listdir("musica/sc/")[1]
	print("mv "+ shlex.quote(os.path.join("musica","sc",dled)) + " " + shlex.quote(os.path.join("musica","sc","mandadas",dled)))
	os.system("mv "+ shlex.quote(os.path.join("musica","sc",dled)) + " " + shlex.quote(os.path.join("musica","sc","mandadas",dled)))

	onSongDl("sc/mandadas/"+dled,dled[:-4],author)


def getSC(link,author):

	thread3 = threading.Thread(target= bajarSC,args=(link,author))
	thread3.start()
	return



#MingoCode hasta aca ------------------------------------------------------------------------------------------------------------------------------------------------


@client.event
async def on_message(message):
	# we do not want the bot to reply to itself
	try:
		await client.change_presence(game=discord.Game(name=pretty_pl[0]))
	except:
		pass

	if message.author == client.user:
		return


	if message.content.startswith('$'): #comandos
		command = message.content[1:]
		print ("command: " + command)

		if not message.channel.is_private:
 			last_channel = message.channel
		


		if command.startswith("play"):
			global voice
			if not client.is_voice_connected(message.server):
				voice = await client.join_voice_channel(message.author.voice_channel)
				print ("conectado al voice channel")

			search = command[4:]
			if "https://soundcloud" in search:
				getSC(search,message.author)
				print(search)
				acks = ['OK','Queued','Sir yes sir!','aye aye captain','Roger that','Your wish is my command','Hai hai! desu :3','Oky Doki','Acknowledged']
				msg = random.choice(acks)
				await client.send_message(message.channel,msg + ' ' + message.author.mention )
				return
			print ("searching " + search)
			onSongIn(search,message.author)
			acks = ['OK','Queued','Sir yes sir!','aye aye captain','Roger that','Your wish is my command','Hai hai! desu :3','Oky Doki','Acknowledged']
			msg = random.choice(acks)
			await client.send_message(message.channel,msg + ' ' + message.author.mention )

		elif command.startswith("skip"):
			global votes
			connected = len(voice.channel.voice_members)
			treshold = int(0.60 * connected)
			votes += 1
			if votes >= treshold:
				global player
				player.stop()
				#onSongEnd()
				await client.send_message(message.channel,"Treshols met! skiping song!")
				votes = 0
			else:
				
				await client.send_message(message.channel,str(votes)+'/'+str(treshold) + " votes recieved " + message.author.mention)

		elif command.startswith("fskip"):
				DJ_role = discord.utils.find(lambda r: 'DJ' in r.name , message.author.roles)
				if DJ_role != None:
					global player
					player.stop()
					#onSongEnd()
					await client.send_message(message.channel,"FORCE SKIP RECIEVED")




		elif command.startswith("doitfaggityoloswag"):
			await client.send_message(message.channel,"$gamble all")
		elif command.startswith("list"):
			msg = "The playlist is: \n"
			for item in pretty_pl:
				msg += item + "\n"
			await client.send_message(message.channel,msg)


		elif command.startswith("fku"):
			server = message.server
			global pl
			global pretty_pl
			pl = []
			pretty_pl = []

			if player.is_playing():
				player.stop()

			try:
				player = None
				voice.disconnect()
			except:
				pass


		elif command.startswith("a"):
			text = command[1:]
			global cb
			print(text)
			answer = cb.ask(text)
			print(answer)
			await client.send_message(message.channel,answer + message.author.mention)




		#await client.send_message(message.channel,"invalid command, this is a new version of mingo, wich only supports play skip and pause atm")






@client.event
async def on_ready():
	print('Logged in as')
	print(client.user.name)
	print(client.user.id)
	print('------')

client.run('MjU2OTE2OTU5NDQ5NDQ4NDQ4.CyzIFA.O9-zDl2zffYkkGRI7Ad6fD4HJwU')



