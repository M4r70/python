import numpy as np
import matplotlib.pyplot as plt
import datetime
import operator

values = []
dates = []

fname = 'ari.txt'
asm1Values = []
asm2Values = []

with open(fname) as f:
	content = f.readlines()
	for c in content:
		c = c.split(',')
		c.remove('\n')
		c = map(int,c)
		l1 = []
		l2 = []
		l1.append(c[0])
		l1.append(c[2])
		l2.append(c[1])
		l2.append(c[2])
		asm1Values.append(l1)
		asm2Values.append(l2)
		

asm1Values.sort(key=lambda x: x[1])
asm2Values.sort(key=lambda x: x[1])

categorias = []
for a1 in asm1Values:
	categorias.append(a1[1])
	del a1[1]
for a2 in asm2Values:
	del a2[1]

asm1 = [item for sublist in asm1Values for item in sublist]
asm2 = [item for sublist in asm2Values for item in sublist]


print asm1
print asm2

data = [asm1,asm2]
labels = ['asm1','asm2']




N = len(categorias)
ind = np.arange(N) + 1  # the x locations for the groups
width = 0.35       # the width of the bars

fig, ax = plt.subplots()
colores = 'bgrcmyk'
list(colores)


bars = []
for i in xrange(0,len(data)):
	bar = ax.bar(ind  + width * i ,data[i],width ,color = colores[i])
	bars.append(bar)
	print i

#rects1 = ax.bar(ind, asm1, width, color='r', )

#rects2 = ax.bar(ind+width, asm2, width, color='y', )

# add some text for categorias, title and axes ticks
ax.set_ylabel('ticks')
ax.set_title('tics por implementacion')
ax.set_xticks(ind+width)
ax.set_xticklabels( categorias )

ax.legend( bars, labels )


plt.show()

