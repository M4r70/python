import speech_recognition as sr
import time
import pyttsx
import time
import os
import threading
from espeak import espeak
import subprocess
import commands
import os




espeak.synth('all, systems, online')

def play_mp3(path):
    subprocess.Popen(['mpg123', '-q', path]).wait()

class Alarm(threading.Thread):
    def __init__(self, hours, minutes):
        super(Alarm, self).__init__()
        self.hours = int(hours)
        self.minutes = int(minutes)
        self.keep_running = True

    def run(self):
        try:
            while self.keep_running:
                now = time.localtime()
                if (now.tm_hour == self.hours and now.tm_min == self.minutes):
                    print("ALARM NOW!")
                    play_mp3('alarm.mp3')
                    return
            time.sleep(60)
        except:
            return
    def just_die(self):
        self.keep_running = False


def despertador(command):
	print 'despertador activado'
	
	command = command.split(':')
	command = ' '.join(command)
	commandL = command.split(' ')
	
	print commandL
	action = 'ERROR EN DESPERTADOR'
	if command.startswith('computer wake me up at'):

		hh = int(commandL[5])
		if commandL[6] == 'am' or commandL[6] == 'pm' or commandL[6] == 'p.m' or commandL[6] == 'a.m' or commandL[6] == 'p.m.' or commandL[6] == 'a.m.' :
			mm = 0
			amPm = commandL[6]
		else:
			mm = int(commandL[6])
			amPm = str(commandL[7])
		if amPm == 'pm' or amPm == 'p.m' or amPm == 'p.m.':
			hh = hh + 12
		action = 'alarm set at : ,' +  str(hh) + ' ' + str(mm) 
	if command.startswith('computer wake me up in'):
		print 'entre2'
		numero = int(commandL[5])
		unidad = str(commandL[6])
		print unidad
		hh = time.localtime().tm_hour
		mm = time.localtime().tm_min

		if unidad == 'hour' or unidad == 'hours':
			hh = time.localtime().tm_hour + numero
		if unidad == 'minute' or unidad == 'minutes':
			mm = time.localtime().tm_min + numero
		action = 'alarm will sound in ,' + str(numero) + ' ,' + unidad 
	print action
	espeak.synth( action )
	alarm = Alarm(hh, mm)
	alarm.start()
	listen()
	
	
def say(command):
	commandL = command.split(' ')
	commandL = commandL[2:len(commandL)]
	print commandL
	command = ' '.join(commandL)
	print command
	espeak.synth(command)
	listen()


def ytaudio(command):
	commandL = command.split(' ')[2:]
	search = ' '.join(commandL)
	espeak.synth('playing ' + search )
	link = commands.getstatusoutput('python3 yt.py ' + search)[1]
	print link
	
	os.system('iceweasel ' + link)
	
	listen()



def listen():
	r = sr.Recognizer()
	with sr.Microphone() as source:  
		#r.adjust_for_ambient_noise(source)
		espeak.synth('listening for commands')
		while  1 == 1:  
			audio = r.listen(source)                   # listen for the first phrase and extract it into audio data
			try:
				
				command = r.recognize(audio)
				print command
				if command.startswith('computer wake me up'):
					despertador(command)
				if command.startswith('computer say'):
					say(command)
				if command.startswith('computer play'):
					ytaudio(command)
				if command.startswith('computer mute') or command.startswith('computer shut up'):
					os.system('killall iceweasel')
				if command.startswith('computer f*** off'):
					espeak.synth('shuting down NOW')
					quit()
				if command.startswith('computer update'):
					os.system('git pull')
					espeak.synth('resetting in 20 secconds to update')
					time.sleep(20) 
					quit()
				if command.startswith('computer sleep'):
					espeak.synth('turning off screen')
					os.system('xset dpms force off')
				
			
			except LookupError:                            # speech is unintelligible
				print("Could not understand audio")
			time.sleep(0.1) 

#MAIN:
listen()
