import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict
import matplotlib.cm as cmx
import matplotlib.colors as colors
from pylab import grid

def get_cmap(N):
    '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
    RGB color.'''
    color_norm  = colors.Normalize(vmin=0, vmax=N-1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='hsv') 
    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color




#OJO AL PIOJO!!! altura_barras = [[altura de la primer barra de cada categoria], [altura de cada barra de la 2da categoria],[etc]]



altura_barras = [[22,16,18],[48,88,35],[55,43,17],[48,25,68]]
nombres_grupos = ['Grupo 1','Grupo 2','Grupo 3']
nombres_barras = ['La 12','Borrachos del Tablon','Guardia Imperial','La Barra Del Loco Tito']
nombre_eje_y = 'Nombre eje y'
titulo = 'Titulo'
width = 0.2

fig = plt.figure()
ax = fig.add_subplot(111)


ind = np.arange(len(nombres_grupos)) 
cmap = get_cmap(len(nombres_barras)+1)
bars = []
for i in xrange(len(altura_barras)) :
	col = cmap(i)
	bar = ax.bar(ind  + width * i ,altura_barras[i],width ,facecolor=col)
	bars.append(bar)

		 
ax.set_ylabel(nombre_eje_y)
ax.set_title(titulo)
ax.set_xticks(ind+width*len(altura_barras)/2)
ax.set_xticklabels(nombres_grupos)
ax.legend( bars, nombres_barras )
grid(True)
plt.show()
